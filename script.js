const keyToggler = document.querySelector('.btn-wrapper');
const keys = {
    Enter: 'Enter',
    s: 's',
    e: 'e',
    o: 'o',
    n: 'n',
    l: 'l',
    z: 'z',
}
let previousKey;
document.addEventListener('keydown', (e) => {
    if(keys[`${e.key}`]) {
    if(previousKey) previousKey.classList.remove('blue');
    document.querySelector(`[data-key="${e.key}"]`).classList.add('blue');
    previousKey = document.querySelector(`[data-key="${e.key}"]`);
    }
});




